" mapping commands in normal mode
"---------------------------------

" Relative or absolute number lines
function! NumberToggle()
    if(&nu == 1)
        set nu!
        set rnu
    else
        set nornu
        set nu
    endif
endfunction

function! SpellToggle()
    if(&spell == 1)
        set nospell
    else
        set spell
    endif
endfunction


"nnoremap <C-r> :call NumberToggle()<CR>
nmap ´R :call NumberToggle()<CR>
"complete with dictionary
imap ´D <C-x><C-k>
"run prettier TODO move to plugins-keys
nmap ´P :Prettier<CR>

"turn on the spellchecker
nmap ´S :call SpellToggle()<CR>



