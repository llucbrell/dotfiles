"-------------------------------------------------------------------------------------
"                 KEY MAPPING
"--------------------------------------------------------------------------------------
"
" Disable Arrow keys in normal, visual, selecte and operator mode
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
" keymapping for control charcters
map! ´ñ <C-V>241
map! ñ <Esc>
vmap ñ <Esc>
omap ñ <Esc>
"map ´´ ´
"map `` `

"numbers to speed up, only in normal mode
"-----------------------------------------
nmap ´q 1
nmap ẃ 2
nmap é 3
nmap ŕ 4
nmap ´t 5
nmap ý 6
nmap ú 7
nmap í 8
nmap ó 9
nmap ṕ 0
" to ensure it works well in most keyboards
nmap ´w 2
nmap ´r 4
nmap ´y 6
nmap ú 7
nmap í 8
nmap ó 9
nmap ´p 0
"numbers to speed up, only in visual mode
vmap ´q 1
vmap ẃ 2
vmap é 3
vmap ŕ 4
vmap ´t 5
vmap ý 6
vmap ú 7
vmap í 8
vmap ó 9
vmap ṕ 0
" to ensure it works well in most keyboards
vmap ´w 2
vmap ´r 4
vmap ´y 6
vmap ú 7
vmap í 8
vmap ó 9
vmap ´p 0
"numbers to speed up, only in command or pending mode
omap ´q 1
omap ẃ 2
omap é 3
omap ŕ 4
omap ´t 5
omap ý 6
omap ú 7
omap í 8
omap ó 9
omap ṕ 0
" to ensure it works well in most keyboards
omap ´w 2
omap ´r 4
omap ´y 6
omap ú 7
omap í 8
omap ó 9
omap ´p 0

"special keys for normal mode
"-----------------------------
nmap ḱ (
nmap ´k (
nmap ĺ )
nmap ´l )
nmap ǘ ] 
nmap ć [
nmap ´v ] 
nmap ´c [
nmap ´j /
nmap ´h &
nmap ǵ %
nmap ´g %
nmap ´f $
nmap ´d #
nmap ´s "
nmap ś "
nmap á !
nmap ź \|
nmap ´z \|
nmap ´x @
nmap ´b \
nmap ń {
nmap ḿ }
nmap ´n {
nmap ´m }
nmap ´, ;
nmap ´. :
nmap ´- _
nmap ´< >
nmap ´+ *
"special keys for pending-mode
omap ḱ (
omap ´k (
omap ĺ )
omap ´l )
omap ǘ ] 
omap ć [
omap ´v ] 
omap ´c [
omap ´j /
omap ´h &
omap ǵ %
omap ´g %
omap ´f $
omap ´d #
omap ´s "
omap ś "
omap á !
omap ź \|
omap ´z \|
omap ´x @
omap ´b \
omap ń {
omap ḿ }
omap ´n {
omap ´m }
omap ´, ;
omap ´. :
omap ´- _
omap ´< >
omap ´+ *
"special keys for visual select mode
vmap ḱ (
vmap ´k (
vmap ĺ )
vmap ´l )
vmap ǘ ] 
vmap ć [
vmap ´v ] 
vmap ´c [
vmap ´j /
vmap ´h &
vmap ǵ %
vmap ´g %
vmap ´f $
vmap ´d #
vmap ´s "
vmap ś "
vmap á !
vmap ź \|
vmap ´z \|
vmap ´x @
vmap ´b \
vmap ń {
vmap ḿ }
vmap ´n {
vmap ´m }
vmap ´, ;
vmap ´. :
vmap ´- _
vmap ´< >
vmap ´+ *
"special keys for select and substitute modes
map! ḱ (
map! ´k (
map! ĺ )
map! ´l )
map! ǘ ] 
map! ć [
map! ´v ] 
map! ´c [
map! ´j /
map! ´h &
map! ǵ %
map! ´g %
map! ´f $
map! ´d #
map! ´s "
map! ś "
"map! á !
map! ź \|
map! ´z \|
map! ´x @
map! ´b \
map! ń {
map! ḿ }
map! ´n {
map! ´m }
map! ´, ;
map! ´. :
map! ´- _
map! ´< >
map! ´+ *

"mapping text objects
"---------------------
omap iì i( 
omap iò i) 
omap ić i[ 
omap iǜ i] 
omap iè i} 
omap iẁ i{ 
omap ic i' 
omap ix i" 
omap iz i> 

omap aì a( 
omap aò a) 
omap aǘ a] 
omap ać a[ 
omap aè a} 
omap aẁ a{ 
omap ac a' 
omap ax a" 
omap az a> 


