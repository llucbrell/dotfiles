"
"
"      dP                dP       dP                                                          dP         
"      88                88       88                                                          88        
"      88d888b. .d8888b. 88d888b. 88d888b. .d8888b. .d8888b. .d8888b. .d8888b. .d8888b. .d888b88 .d8888b.
"      88'  `88 88'  `88 88'  `88 88'  `88 88ooood8 Y8ooooo. 88'  `"" 88'  `"" 88'  `88 88'  `88 88ooood8
"      88    88 88.  .88 88.  .88 88.  .88 88.  ...       88 88.  ... 88.  ... 88.  .88 88.  .88 88.  ...
"      dP    dP `88888P' 88Y8888' 88Y8888' `88888P' `88888P' `88888P' `88888P' `88888P' `88888P8 `88888P'
"                                                                                                  
"                                                                                           
"                                  i8@@8Li.                t@8000                                 
"                                .GitL:::GGGC;             G8ifGt80LtL11,.                        
"                                 C,Li;i8i;Ci:ti  i11i:;i::i00ii;;ifffi;;f@8                      
"                                 ,L;0L88iCf;L01f.   ,;  ifG0101f0t:;:;;;1GC                      
"                                   t@80CL0LfC,    ,1i :;ii CGti1;G8fi::fCiL:                     
"                                     L0C11tGf;      ,1;   L .;:.         Lii                     
"                                     ,i0fLGt;f1.    t:                    1.                     
"                                   ;0C8Li;t88L;:,,i  :t,          :1 .,  .t                      
"                                 18i;i;1fC8tL,      .             ,  1. :L                       
"                               1C:;G8Gf;f;             ::::      ;LG0t. iLG0CCi                  
"           ..t0fC0tGt,       iC:ifCGCt:             ;,    ;L  i G1G0C11CLCL1C88CGti              
"         ,Li;G01f01C::GC.   C8G1;:;:;              t           ii8@@@@@C  ;G81iGfitCtt1tf:       
"       .CiGtL,      :0:;8: Gi:i08@8.           ..                .ifft,       iL:.       .1      
"      .0CLfi          Ct;;08Cffti1.          .,                                f.  i   it:       
"      t1i:L            0GLf:::;;;i          ;.                                 i  :.  ;   ::     
"      0@@@;            ,iGt:::;:1          f                                    ;; ,i  i1ti      
"      C@@@.             18888@@8C         .f     :t11:                               t. i        
"      .8@0               G;:::;C8f      :;.L   ,i   , f                                          
"                         ff:C8L;;;: t81,:8::f..f    @1 1                                         
"                         G0i:::i8L10@8,,ff,f0L0. t@,.f.1                                         
"                          C1:18L:i8;88,:Lf8t:f:   : Lt:.                                         
"                          ,80C:::8t:L8f:G@G,:L   ,@8  i   ;i.                                    
"                           ,8:::t8i::C8C1@8CfG   f@G i ;;    ;1                                  
"                             tC:t8f::,;C8C1L0f     .G1f..1 G@,1                                  
"                               i0t01::::::f01,f1,iG0;1  Gf    1                                  
"                                  :tC00Lt:.      ;G,       8L1.          
"                         .LG:;                    f.  f@@1  t.           
"                     ,;.0ttC::::                   L   :t: ,:            
"                i;ftttttttt8:::;                    .L.  :1.             
"            .:Gtttttfttttttttf;::;                                       
"        i8tttttft::Cttttttttt;::::                                       
"        tttC,.;;:::::fttttttttt::::;                                     
"      ,::1Lt8;:::::::tttttttttt;:::f                                     
"    8:0tttttttG;::::::;tttttttttt:::;                                    
"  CtfttttttttttG::::::::tttttttttt:::i                                   
"  ttttttttttttttt:::::::;tttttttttC:::                                   
"   Gtttttttttttttt:::::::Gtttttttttf::t                                  
"   .ttttttttttttttt:L.Cii;tttttttttL;f1                                  
"    Lttttttttttttttfift,8ttttttttff           
"     Gttttttttttttt0fttttttttt,              
"      tttttttt1GGttCtLtG8G                  
"       .1ft.i,.  t18i           GGGf;;;:::::;::i;:;C 
"               t          .ifi:::;:;1fft11if;L11LLf1itC8fi 
"                  ;  C8LttttCCLttttttttG1ft1f       
"                  :         t                      
"                 ;         ;                 Template-name    vimrc-file		
"                L        t                
"              C       t                     	    Author	 Lucas_C/llucbrell/hobbescode       
"                8    C                       
"                ;Ct                          	    License    	GNU-license       
"
"---------------------------------------------------------------------------------------------
"                                BASIC STYLE FOR VIM
"---------------------------------------------------------------------------------------------

"filetype detection
filetype plugin indent on

" activates syntax highlighting among other things
syntax on

" allows you to deal with multiple unsaved
" buffers simultaneously without resorting
" to misusing tabs
set hidden

" just hit backspace without this one and
" see for yourself
set backspace=indent,eol,start

" set number of lines
set number
" unset numbers en raltion to the cursor
set norelativenumber

" set visual line on the cursor
set cursorline 
"hi CursorLine guibg=lightgray ctermbg=black ctermfg=red
hi CursorLine guibg=lightgray ctermbg=black 

"Change Color when entering Insert Mode
"autocmd InsertEnter * highlight  CursorLine ctermbg=lightgray ctermfg=red
" with this change we can see color schemes while we type
autocmd InsertEnter * highlight  CursorLine ctermbg=lightgray

" Revert Color to default when leaving Insert Mode
"autocmd InsertLeave * highlight  CursorLine ctermbg=black ctermfg=red
" with this change we can see color schemes while we type
autocmd InsertLeave * highlight  CursorLine guifg=black ctermbg=black

" show commands while typing in normal mode
set showcmd

" set the color scheme
"colorscheme default
colorscheme desert

" turn on the spellchecker for spanish and english
" also allways to show only 5 suggestions
set spelllang=en,es

" set the completition configuration
" to look at the current buffer, the windows, the unloaded buffers
" the tabs, the included files, and the spellchecker only when
" its turn on
set complete=.,w,b,u,t,i,kspell

" put a new line character at specified position in all lines
" also called wrapmargin at 20 characters
" set wrapmargin=20 

" no swap files and backups
"set nobackup       "no backup files
"set nowritebackup  "only in case you don't want a backup file while editing
set noswapfile     "no swap files

" adjust colors to have better contrast
"set background=dark
"set t_Co=256


"--------------------------------------------------------------------------- 
"                          VIM MAPPINGS and OTHER
"---------------------------------------------------------------------------
" add basic key mapping
runtime .vimlc/basic-map.vim
" add leader key mapping
runtime .vimlc/leader-map.vim
" add special predefined actions to keys -- used for complex or ide like actions
runtime .vimlc/upper-actions-map.vim
" if it's neovim
runtime .vimlc/neovim.vim




